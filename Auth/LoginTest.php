<?php

namespace Tests\Browser\Pages\Auth;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLoginWithWrongPassword()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email', 'longtest121@mail.com')
                ->type('password', '987654321')
                ->click('.page-title')
                ->pause(1000)
                ->click('#login-btn');

            $browser->waitForText('入力情報に誤りがあります');
        });
    }

    public function testLoginWithNotExistEmail()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email',$email)
                ->type('password', '987654321')
                ->click('.page-title')
                ->pause(1000)
                ->click('#login-btn');

            $browser->waitForText('入力情報に誤りがあります');
        });
    }

    public function testLoginWithWrongFormatEmail()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = "long@mailcom";
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email',$email)
                ->type('password', '987654321')
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('メールアドレスは、有効なメールアドレス形式で指定してください。')
                ->assertAttribute('#login-btn','disabled','true');
        });
    }

    public function testLoginWithUserState1()
    {
        $this->browse(function (Browser $browser) {
            $email = "long@mail.com";
            $password = "12345678";
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email',$email)
                ->type('password', $password)
                ->click('.page-title')
                ->pause(1000)
                ->click('#login-btn');

            $browser->assertPathIs('/');
        });
    }

    public function testLoginWithUserState0()
    {
        $this->browse(function (Browser $browser) {
            $email = "test1301@mail.com";
            $password = "12345678";
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email',$email)
                ->type('password', $password)
                ->click('.page-title')
                ->pause(1000)
                ->click('#login-btn');

            $browser->waitForText('14日間無料体験お申込み')
                    ->assertPathBeginsWith('/signup')
                    ->assertValue('input[name="last_name"]', "aaa")
                    ->assertValue('input[name="first_name"]', "bbb")
                    ->assertValue('input[name="tel"]', "0987654321");
        });
    }

    public function testLoginWithUserState0V2()
    {
        $this->browse(function (Browser $browser) {
            $email = "test1301v2@mail.com";
            $password = "12345678";
            $browser->visit('/')
                ->assertSee('ログイン')
                ->type('email',$email)
                ->type('password', $password)
                ->click('.page-title')
                ->pause(1000)
                ->click('#login-btn');

            $browser->waitForText('登録お申込みページ')
                ->assertPathBeginsWith('/signup/v2')
                ->assertValue('input[name="last_name"]', "last")
                ->assertValue('input[name="first_name"]', "first")
                ->assertValue('input[name="company_name"]', "longcompany")
                ->assertValue('input[name="tel"]', "0987654321");
        });
    }
}
