<?php

namespace Tests\Browser\Pages\Auth;

use Faker\Factory;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class SignUpV1Test extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testGoToSignUpPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/signup')
                ->assertSee('14日間無料体験お申込み')
                ->assertPresent('input[name="email"]')
                ->assertPresent('input[name="password"]')
                ->assertPresent('input[name="last_name"]')
                ->assertPresent('input[name="tel"]');
        });
    }

    public function testV1Personal()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = '12345678';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('.page-title')
                ->pause(1000)
                ->script("document.getElementById('btn-user-info').click();");
//            $browser->pause(1000)->script("document.getElementById('btn-user-info').click();");
//                ->click('.page-title')
//                ->pause(1000)
//                ->click('.btn-primary');
            $browser->screenshot('SignUpV1');
            $browser->waitForText('以下の内容で14日間無料体験申し込みを行います')
                ->assertSee($lastName)
                ->assertSee($firstName)
                ->assertSee($email)
                ->assertSee($tel)
                ->script("document.getElementById('input-confirm').click();");

            $browser->pause(1000)
//                ->click('#btn-confirm-data');
            ->script("document.getElementById('btn-confirm-data').click();");

            $browser->assertPathIs('/');
            $user = \DB::table('users')->where('email',$email)->first();
            $this->assertEquals($user->email,$email);
            $this->assertEquals($user->tel,$tel);
            $this->assertEquals($user->first_name,$firstName);
            $this->assertEquals($user->last_name,$lastName);
            $this->assertEquals($user->state,1);
            $this->assertEquals($user->account_type,0);
        });
    }

    public function testV1Company()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = '12345678';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $companyName = $faker->company;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('#company-btn')
                ->waitForText('会社名')
                ->type('company_name', $companyName)
                ->click('.page-title')
                ->pause(1000)
                ->script("document.getElementById('btn-user-info').click();");

            $browser->waitForText('以下の内容で14日間無料体験申し込みを行います')
                ->assertSee($lastName)
                ->assertSee($firstName)
                ->assertSee($email)
                ->assertSee($tel)
                ->script("document.getElementById('input-confirm').click();");

            $browser->pause(1000)
                ->click('#btn-confirm-data');

            $browser->assertPathIs('/');
            $user = \DB::table('users')->where('email',$email)->orderBy('id','DESC')->first();
            $this->assertEquals($user->email,$email);
            $this->assertEquals($user->tel,$tel);
            $this->assertEquals($user->first_name,$firstName);
            $this->assertEquals($user->last_name,$lastName);
            $this->assertEquals($user->company_name,$companyName);
            $this->assertEquals($user->state,1);
            $this->assertEquals($user->account_type,1);
        });
    }

    public function testV1WrongEmailFormatCompany()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = "aaaabbbb@mailcom";
            $password = '12345678';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $companyName = $faker->company;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('#company-btn')
                ->waitForText('会社名')
                ->type('company_name', $companyName)
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('メールアドレスは、有効なメールアドレス形式で指定してください。')
                ->assertAttribute('#btn-user-info','disabled','true');

        });
    }

    public function testV1WrongPasswordFormatPersonal()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = "aaaabbbb@mail.com";
            $password = '12345678901234567890123456';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('パスワードは、24文字以下にしてください。')
                ->assertAttribute('#btn-user-info','disabled','true');

            $browser->type('password', 'abc123')
                ->assertSee('パスワードは、8文字以上にしてください。')
                ->click('.page-title')
                ->pause(1000)
                ->assertAttribute('#btn-user-info','disabled','true');
        });
    }

    public function testV1WrongLastNameFormatPersonal()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = '12345678';
            $lastName = 'nam1';
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('姓には、有効な正規表現を指定してください。')
                ->assertAttribute('#btn-user-info','disabled','true');
        });
    }

    public function testV1WrongFirstNameFormatCompany()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = '12345678';
            $lastName = $faker->lastName;
//            $firstName = $faker->firstName;
            $companyName = $faker->company;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', 'táo')
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('#company-btn')
                ->waitForText('会社名')
                ->type('company_name', $companyName)
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('名には、有効な正規表現を指定してください。')
                ->assertAttribute('#btn-user-info','disabled','true');

        });
    }

    public function testV1WrongTelFormatCompany()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = '12345678';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $companyName = $faker->company;
            $tel = '09881212';
            $browser->visit('/signup')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('#company-btn')
                ->waitForText('会社名')
                ->type('company_name', $companyName)
                ->click('.page-title')
                ->pause(1000)
                ->assertSee('電話番号には、有効な正規表現を指定してください。')
                ->assertAttribute('#btn-user-info','disabled','true');
        });
    }
}
