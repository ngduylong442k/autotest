<?php

namespace Tests\Browser\Pages\Auth;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class SignUpV2Test extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSignUpV2()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = $faker->email;
            $password = $faker->password(8, 16);
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $companyName = $faker->company;
            $tel = $faker->regexify('09[0-9]{9}');
            $browser->visit('/signup/v2')
                ->type('email', $email)
                ->type('password', $password)
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('tel', $tel)
                ->click('#company-btn')
                ->waitForText('会社名')
                ->type('company_name', $companyName)
//                ->click('.page-title')
//                ->pause(1000)
//                ->click('.btn-primary');
                ->pause(1000)
                ->script("document.getElementById('btn-user-info').click();");

            $browser->waitForText('プラン選択')
                ->assertSee('プラン選択')
                ->clickLink('1ヶ月')
                ->pause(1000)
                ->click('#collapse2-1 > div.buttons.min-height-71 > button')
                ->waitForText('確認')
                ->press('確認');

            $browser->whenAvailable('#modal-select-payment-method', function ($modal) {
                $modal->assertSee('毎月のお支払い方法をご選択ください')
                    ->radio('select_payment_method', 'paygent')
                    ->press('次へ');
            });

            $browser->waitForText('お支払いに利用するクレジットカード情報')
                ->assertSee('お支払いに利用するクレジットカード情報')
                ->type('card_number', '4242424242424242')
                ->type('cvc', '123')
                ->select('expire_month', '12')
                ->select('expire_year', '23')
                ->waitForText('次へ')
                ->press('次へ');

            $browser->pause(10000)
//            $browser->waitForText('以下の内容で登録を申し込みます。よろしいですか',15)
                ->assertSee($lastName)
                ->assertSee($firstName)
                ->assertSee($email)
                ->assertSee($companyName)
                ->assertSee($tel)
                ->script("document.getElementById('input-confirm').click();");

            $browser->waitForText('決済に進む')
                ->press('決済に進む');

            $browser->whenAvailable('#modal-plan-register', function ($modal) {
                $modal->assertSee('プラン料金決済')
                    ->assertSee('※ご請求金額には、消費税と決済手数料4%が含まれています。')
                    ->pressAndWaitFor('決済を完了してアドスタをはじめる');
            });

            $browser->assertPathIs('/');
        });
    }

    public function testV2ExistEmailV2State0()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = "test1301v2@mail.com";
            $password = '0987654321';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup/v2')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('.page-title')
                ->pause(1000)
                ->click('#btn-user-info');

            $browser->assertPathIs('/signup/v2/select_plan');
            $user = \DB::table('users')->where('email',$email)->first();
            $this->assertEquals($user->email,$email);
            $this->assertEquals($user->tel,$tel);
            $this->assertEquals($user->first_name,$firstName);
            $this->assertEquals($user->last_name,$lastName);
            $this->assertEquals($user->state,0);
            $this->assertEquals($user->account_type,0);
        });
    }

    public function testV2ExistEmailV1State0()
    {
        $this->browse(function (Browser $browser) {
            $faker = Factory::create();
            $email = "test1301@mail.com";
            $password = '0987654321';
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->visit('/signup/v2')
                ->type('last_name', $lastName)
                ->type('first_name', $firstName)
                ->type('email', $email)
                ->type('password', $password)
                ->type('tel', $tel)
                ->click('.page-title')
                ->pause(1000)
                ->click('#btn-user-info');

            $browser->assertPathIs('/signup/v2');
        });
    }
}
