<?php

namespace Tests\Browser\Pages;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class MyPageTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function visitMyPage(Browser $browser, $email)
    {
        $browser->visit('/')
            ->type('email', $email)
            ->type('password', '12345678')
            ->click('.page-title')
            ->pause(1000)
            ->click('#login-btn');

        $browser->visit('/my_page');
    }

    public function testMyPageInfo()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long@mail.com';
            $this->visitMyPage($browser, $email);

            $browser->assertPathIs('/my_page');

            $user = \DB::table('users')->where('email', $email)->first();
            $browser->visit('/my_page')
                ->assertValue('input[name="email"]', $email)
                ->assertValue('input[name="last_name"]', $user->last_name)
                ->assertValue('input[name="first_name"]', $user->first_name)
                ->assertValue('input[name="tel"]', $user->tel);

            $browser->click('#payment-tab')
                ->waitForText('お申し込み中のプラン・コース')
                ->assertSee('現在のお支払い方法')
                ->assertSee('4242');

            $browser->clickLink('SNS')
                ->waitForText('Facebookページ')
                ->assertPresent('input[id="name-user-facebook"]')
                ->assertPresent('input[id="url-user-facebook"]');
        });
    }

    public function testChangeInfoUser()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->assertValue('input[name="email"]', $email)
                ->type('input[name="last_name"]', $lastName)
                ->type('input[name="first_name"]', $firstName)
                ->type('input[name="tel"]', $tel)
                ->type('input[name="current_password"]', '12345678')
                ->type('input[name="new_password"]', '12345678')
                ->type('input[name="new_confirm_password"]', '12345678')
                ->click('#btn-submit');

            $browser->pause(5000)
                ->assertPathIs('/my_page');

            $user = \DB::table('users')->where('email', $email)->first();
            $this->assertEquals($user->email, $email);
            $this->assertEquals($user->tel, $tel);
            $this->assertEquals($user->first_name, $firstName);
            $this->assertEquals($user->last_name, $lastName);
        });
    }

    public function testTypeIncorrectCurrentPassword()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->assertValue('input[name="email"]', $email)
                ->type('input[name="last_name"]', $lastName)
                ->type('input[name="first_name"]', $firstName)
                ->type('input[name="tel"]', $tel)
                ->type('input[name="current_password"]', $faker->password)
                ->type('input[name="new_password"]', '12345678')
                ->type('input[name="new_confirm_password"]', '12345678')
                ->pause(1000)
                ->assertSee('パスワードが正しくありません。');

        });
    }

    public function testTypeRePassNotMatchPass()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $lastName = $faker->lastName;
            $firstName = $faker->firstName;
            $tel = $faker->regexify('09[0-9]{8}');
            $browser->assertValue('input[name="email"]', $email)
                ->type('input[name="last_name"]', $lastName)
                ->type('input[name="first_name"]', $firstName)
                ->type('input[name="tel"]', $tel)
                ->type('input[name="current_password"]', '12345678')
                ->type('input[name="new_password"]', '12345678')
                ->type('input[name="new_confirm_password"]', '12345678abc')
                ->pause(1000)
                ->assertSee('新しいパスワードと新しいパスワード（確認）が一致しません。');
        });
    }

    public function testRegisterUserFacebook()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $name = $faker->name;
            $url = 'https://www.facebook.com/test' . $faker->userName;
            $browser->clickLink('SNS')->waitForText('Facebookページ')
                ->type('input[id="name-user-facebook"]', $name)
                ->type('input[id="url-user-facebook"]', $url)->screenshot('my_page')
                ->pause(1000)
                ->click('#btn-create-user-facebook');

            $browser->pause(10000);
            $user = \DB::table('users')->where('email', $email)->first();
            $userFacebook = \DB::table('user_facebooks')->where('user_id', $user->id)
                ->orderBy('id', 'DESC')->first();
            $this->assertEquals($userFacebook->name, $name);
            $this->assertEquals($userFacebook->facebook_url, $url);
        });
    }

    public function testRegisterUserFacebookWithWrongUrl()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $name = $faker->name;
            $url = 'https://www.facebook/' . $faker->userName;
            $browser->clickLink('SNS')->waitForText('Facebookページ')
                ->type('input[id="name-user-facebook"]', $name)
                ->type('input[id="url-user-facebook"]', $url)
                ->pause(1000)
                ->assertSee('FacebookページURLには、有効な正規表現を指定してください。');
        });
    }

    public function testRegisterUserRetarget()
    {
        $this->browse(function (Browser $browser) {
            $email = 'long121@mail.com';
            $this->visitMyPage($browser, $email);

            $faker = Factory::create();
            $name = $faker->name;
            $url = 'https://www.facebook.com/test' . $faker->userName;
            $browser->clickLink('SNS')->waitForText('リターゲティング')
                ->select('type')
                ->screenshot('my_page')
                ->pause(1000);
//                ->click('#btn-create-user-facebook');
//
//            $browser->pause(10000);
//            $user = \DB::table('users')->where('email', $email)->first();
//            $userFacebook = \DB::table('user_facebooks')->where('user_id', $user->id)
//                ->orderBy('id', 'DESC')->first();
//            $this->assertEquals($userFacebook->name, $name);
//            $this->assertEquals($userFacebook->facebook_url, $url);
        });
    }
}
